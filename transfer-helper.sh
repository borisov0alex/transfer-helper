#!/bin/sh
if [ -z "$(git config hooks.transfer-helper)" ]; then exit 0; fi

git rev-parse --verify --quiet CHERRY_PICK_HEAD2
if [ $? -eq 0 ]; then PICK_HEAD=$(git rev-parse CHERRY_PICK_HEAD2); git update-ref -d refs/CHERRY_PICK_HEAD2; fi
git rev-parse --verify --quiet CHERRY_PICK_HEAD
if [ $? -eq 0 ]; then PICK_HEAD=$(git rev-parse CHERRY_PICK_HEAD); fi
if [ -z "${PICK_HEAD}" ]; then exit 0; fi

if [ "$(git config hooks.transfer-helper)" = "remote" ] && [ $(git branch --contains ${PICK_HEAD} | wc -l) -ne 0 ]; then     
    git branch --contains ${PICK_HEAD}
    echo "Transfer helper: ${PICK_HEAD} is in non remote branch. Exit"; 
    exit 0; 
fi

echo "Transfer helper: create commits"
BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)

git commit --no-verify -F $1
NEW_HEAD=$(git rev-parse HEAD)
echo "Transfer helper: commit ${NEW_HEAD}"
git reset --hard HEAD^
git cherry-pick ${PICK_HEAD}
git add .
git commit --no-verify -m "Transfer without conflict resolution from ${PICK_HEAD}"
echo "Transfer helper: cherry-pick commit ${PICK_HEAD} with conflicts"
git checkout ${NEW_HEAD}
git reset --soft ${BRANCH_NAME}
git checkout ${BRANCH_NAME}
git commit --no-verify -C ${NEW_HEAD}
echo "Transfer helper: redo commit ${NEW_HEAD}"
exit 1
