# Скрипт для удобного переноса коммитов

## Установка

Установка нужна для каждого репозитория, в который нужно переносить коммиты.

1. Положить remember-cherry-pick.sh и transfer-helper.sh в папку .git в репозитории
2. Сделать их исполняемыми `chmod +x remember-cherry-pick.sh` `chmod +x transfer-helper.sh`. Для Windows не надо.
3. Добавить в .git/hooks/commit-msg строку `.git/transfer-helper.sh $@` сразу после строки `#!/bin/sh`.
   Если файла .git/hooks/commit-msg нет, создать и написать в нём
    ```
    #!/bin/sh
    .git/transfer-helper.sh $@
    ```
   И сделать исполняемым, как в п. 2
4. Аналогично, добавить в .git/hooks/post-checkout строку `.git/remember-cherry-pick.sh $@` сразу после `#!/bin/sh`.
   Если файла нет, создать и написать в нём
    ```
    #!/bin/sh
    .git/remember-cherry-pick.sh $@
    ```
   Сделать исполняемым, как в п. 2
5. Перезапустить IDE

## Использование

Пусть требуется перенести коммит из репозитория c:/projects/bombshell/frontend-client из ветки dev
в c:/projects/fanzter/frontend-client в ветку feature/CO-123/transfer

1. Открыть c:/projects/fanzter/frontend-client в Webstorm,
   зайти во вкладку Terminal.
2. Добавить новый remote, если его ещё нет, для этого выполнить 
   ```
   git remote add localbombshell c:/projects/bombshell/frontend-client
   ```
3. Добавить или актуализировать ветку dev, из которой переносим
   ```
   git fetch localbombshell dev
   ```
4. Включить скрипт перед переносом коммитов
   ```
   git config hooks.transfer-helper remote
   ```
   Если команда `git config hooks.transfer-helper` выводит `remote`, то скрипт включён
5. Перейти на ветку feature/CO-123/transfer
6. Выбрать ветку dev из localbombshell  

   ![](https://i.imgur.com/LZ8pHVV.png)
7. Черри-пикнуть нужный коммит из IDE
8. Отказаться от резолва конфликтов сразу
9. Перенести правки в default changelist
10. Разрезолвить конфликты, если есть, починить тесты, что-то ещё добавить и т.п. как обычно
11. Сделать коммит, должны быть включены хуки при коммите, стоять чекбокс Run git hooks

Должны создаться два коммита
