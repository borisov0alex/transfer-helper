#!/bin/sh
if [ -z "$(git config hooks.transfer-helper)" ]; then exit 0; fi

echo "Transfer helper: try remember ref"

git rev-parse --verify --quiet CHERRY_PICK_HEAD
if [ $? -eq 0 ]; then 
    echo "Transfer helper: remember ref $(git rev-parse CHERRY_PICK_HEAD)"; 
    git update-ref refs/CHERRY_PICK_HEAD2 $(git rev-parse CHERRY_PICK_HEAD); 
fi
